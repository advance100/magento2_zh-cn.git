### 发布到gitee和packagist上
```
// 当前工作目录为 abcd@flower:/var/www/store/meirong/vendor/advance100/magento2_zh-cn$
$ git add .
$ git commit -m "第1次上传"
$ git tag 1.0.0
$ git push origin master --tag
```
在`packagist.org`上提交包
提交uri：`https://packagist.org/packages/submit`，
码云上项目: `https://gitee.com/advance100/magento2_zh-cn.git`
在Repository URL (Git/Svn/Hg)看输入 



### Magento2 简体语言包
持续完善中



### 安装语言包
**Composer安装**
项目根目录为: `/var/www/store/meirong`
```
cd 项目根目录
composer require advance100/magento2_zh-cn:dev-master
php bin/magento cache:clean 
php bin/magento setup:static-content:deploy zh_Hans_CN
```
**手动安装**
- [下载 Magento2 中文包](https://gitee.com/advance100/magento2_zh-cn.git)
- 解压并上传文件到指定目录：项目根目录/app/i18n/advance100/magento2_zh_hans_cn
- 在Magento2根目录执行命令：
```
php bin/magento cache:clean 
php bin/magento setup:static-content:deploy zh_Hans_CN
```
- 登录Magento2管理后台，选择中文语言包：Stores -> Configuration -> General > General -> Locale options -> Chinese (China)


